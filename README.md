# Breton Pronunciation Dictionary
[![License: MIT](https://img.shields.io/badge/License-MIT-blue.svg)](https://opensource.org/licenses/MIT)
[![Discord](https://img.shields.io/discord/718008955040956456.svg?label=&logo=discord&logoColor=ffffff&color=7389D8&labelColor=6A7EC2)](https://discord.gg/UzaFmfV)

An open pronunciation dictionary for Breton. The main file is a tab seperated file containing a list of words and their pronounciation.

## Contributing

The aim is to make this resource as good as possible. If you see any missing words or errors, please feel free to submit a merge request with any changes.

## Related Projects

This resource is made independently from other projects, however, it is maintained by the same people who maintains the [Open Celtic Dictionary project](https://digi.prv.cymru). If you want to stay up to date on the project, feel free to join the [Discord server](https://discord.gg/UzaFmfV).

## Maintainers

* Preben Vangberg &lt;prv21fgt@bangor.ac.uk&gt;

## Acknowledgements
I want to thank everyone who has contributed or who wants to contribute.
I also want to acknowledge the work of the authors of [Wikeriadur](https://br.wiktionary.org) (Breton Wiktionary) as much of the initial parts of this file was gathered from that source.

